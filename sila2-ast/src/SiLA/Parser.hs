{-# language OverloadedStrings #-}
{-# language DerivingStrategies #-}
{-# language LambdaCase #-}
{-# language RecordWildCards #-}

module SiLA.Parser where

import Data.Attoparsec.ByteString (Parser(..))
import Data.Attoparsec.ByteString.Char8 (parseOnly, many1, char, digit, endOfInput, scientific)
import Data.Scientific (toBoundedInteger)
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as C
import Data.List (find)
import Control.Monad (liftM2, zipWithM)
import qualified Data.Text as T
import qualified Data.Text.Encoding as E
import qualified SiLA.AST as AST
import qualified Xeno.DOM as X
import Debug.Trace

data ParseError
  = NodeNotFoundError ByteString
  | NodeNoChildsError
  | MultipleNodesFoundError
  | NodeNoText
  | AttributeNotFoundError X.Node ByteString
  | ParseError String
  deriving stock (Show, Eq)

parseFile :: FilePath -> IO (Either ParseError AST.Feature)
parseFile f = do
  content <- BS.readFile f
  case X.parse content of
    Left e    -> return $ Left (ParseError $ show e)
    Right dom -> return $ parse dom

parse :: X.Node -> Either ParseError AST.Feature
parse root = do
  featureSiLA2Version   <- parseSiLAVersion root
  featureVersion        <- parseFeatureVersion root
  featureIdentifier     <- parseIdentifier root
  featureDisplayName    <- parseDisplayName root
  featureDescription    <- parseDescription root
  featureCommand        <- parseCommands root
  featureProperty       <- parseProperties root
  featureMetadata       <- parseMetadatas root 
  featureDefinedExecutionError <- parseDefinedExecutionErrors root
  featureDataTypeDefinition    <- parseDataTypeDefinition root
  featureMaturityLevel  <- parseMaturityLevel root
  featureOriginator     <- parseOriginator root
  featureCategory       <- parseCategory root
  return AST.Feature{..}



-- 
-- Helpers
--
positiveIntParser :: Parser Int
positiveIntParser = many1 digit >>= (\x -> return $ read x)

withParser :: ByteString -> Parser a -> Either ParseError a
withParser str p = case parseOnly (p <* endOfInput) str of
  Left  s -> Left $ ParseError s
  Right r -> Right r

textParser :: X.Node -> Either ParseError T.Text
textParser node = case getContentText node of
  Left  _ -> Left $ NodeNoText
  Right r -> Right $ E.decodeUtf8 r

findAttr :: ByteString -> X.Node -> Either ParseError ByteString
findAttr attr node = case find (\(a,_) -> a == attr) (X.attributes node) of
  Nothing     -> Left $ AttributeNotFoundError node attr
  Just (_, v) -> Right v  

-- left if more then 1 child with the same name
findChild :: ByteString -> X.Node -> Either ParseError X.Node
findChild name parent = case filter (\n -> X.name n == name) (X.children parent) of
  []        -> Left $ NodeNotFoundError ((name <> " " <> ( (X.name parent))))
  (a:[])    -> Right a
  otherwise -> Left $ MultipleNodesFoundError

findChilds :: ByteString -> X.Node -> Either ParseError [X.Node]
findChilds name parent = Right $ filter (\n -> X.name n == name) (X.children parent)

getContentText :: X.Node -> Either ParseError ByteString
getContentText parent = case find isText (X.contents parent) of
  Nothing         -> Left NodeNoText
  Just (X.Text t) -> Right t

isText :: X.Content -> Bool
isText (X.Text _) = True
isText _          = False

getSingleChild :: X.Node -> Either ParseError X.Node
getSingleChild parent = case X.children parent of
  []        -> Left $ NodeNoChildsError
  (a:[])    -> Right a
  otherwise -> Left $ MultipleNodesFoundError  

parseVersion :: ByteString -> X.Node -> Either ParseError AST.Version
parseVersion needle root = do
  attr <- findAttr needle root
  case parseOnly (versionParser <* endOfInput)  attr of
    Left e  -> Left $ ParseError e
    Right r -> Right r
  where
    versionParser:: Parser AST.Version
    versionParser = do
      major <- many1 digit
      char '.'
      minor <- many1 digit
      return $ AST.Version (read major) (read minor)

parseCommandType :: X.Node -> Either ParseError AST.Observable
parseCommandType parent = do
  cmdTypeNode <- findChild "Observable" parent
  cmdType     <- getContentText cmdTypeNode
  case cmdType of
    "No"  -> Right AST.NonObservable
    "Yes" -> Right AST.Observable
    otherwise -> Left $ ParseError "CommandType not available"
 
parseElement :: X.Node -> Either ParseError AST.Element
parseElement parent = do
  elementIdentifier  <- parseIdentifier parent
  elementDisplayName <- parseDisplayName parent
  elementDescription <- parseDescription parent
  elementDataType    <- parseDataType parent
  return AST.Element{..}

parseListType :: X.Node -> Either ParseError AST.DataType
parseListType parent = do
  childs   <- findChilds "DataType" parent
  listType <- sequence $ parseDataTypeType <$> childs
  Right $ AST.ListType listType

parseStructureType :: X.Node -> Either ParseError AST.DataType
parseStructureType parent = do
  childs <- findChilds "Element" parent
  case childs of
    [] -> Left $ NodeNotFoundError "Should have at least one 'Element' element"
    xs -> AST.StructureType <$> (sequence $ parseElement <$> xs)

parseConstraint :: X.Node -> Either ParseError AST.Constraint
parseConstraint node = 
  case (X.name node) of
    "Length" -> AST.CLength <$> (getContentText node >>= (\c -> withParser c positiveIntParser))
    "MinimalLength" ->
      AST.CMinimalLength <$> (getContentText node >>= (\c -> withParser c positiveIntParser)) 
    "MaximalLength" -> 
      AST.CMaximalLength <$> (getContentText node >>= (\c -> withParser c positiveIntParser))
    "Set" -> (\c -> AST.CSet (E.decodeUtf8 <$> c)) 
      <$> (findChilds "Value" node >>= (\l -> sequence $ getContentText <$> l))
    "Pattern" -> AST.CPattern <$> textParser node
    "MaximalExclusive" -> AST.CMaximalExclusive <$> textParser node
    "MaximalInclusive" -> AST.CMaximalInclusive <$> textParser node
    "MinimalExclusive" -> AST.CMinimalExclusive <$> textParser node
    "MinimalInclusive" -> AST.CMinimalInclusive <$> textParser node
    "Unit" -> AST.CUnit <$> parseUnit node
    "ContentType" -> AST.CContentType <$> parseContentType node
    "ElementCount" -> AST.CElementCount <$> (getContentText node >>= (\c -> withParser c positiveIntParser))
    "MinimalElementCount" -> 
      AST.CMinimalElementCount <$> (getContentText node >>= (\c -> withParser c positiveIntParser))
    "MaximalElementCount" -> 
      AST.CMaximalElementCount <$> (getContentText node >>= (\c -> withParser c positiveIntParser))
    "FullyQualifiedIdentifier" -> AST.CFullyQualifiedIdentifier <$> parseQualifiedIdentifier node
    "Schema" -> AST.CSchema <$> (parseSchemaConstraint node)
    "AllowedTypes" -> do
      childs <- findChilds "DataType" node
      AST.CAllowedTypes <$> sequence (parseDataTypeType <$> childs)
    c -> Left $ ParseError (T.unpack $ "Constraint not known " <> E.decodeUtf8 c)

parseQualifiedIdentifier :: X.Node -> Either ParseError AST.QualifiedIdentifier
parseQualifiedIdentifier root = do
  qf <- textParser root
  case qf of
    "FeatureIdentifier" -> Right AST.FeatureIdentifier
    "CommandIdentifier" -> Right AST.CommandIdentifier
    "CommandParameterIdentifier" -> Right AST.CommandParameterIdentifier
    "CommandResponseIdentifier"  -> Right AST.CommandResponseIdentifier
    "IntermediateCommandResponseIdentifier" -> Right AST.IntermediateCommandResponseIdentifier
    "DefinedExecutionErrorIdentifier" -> Right AST.DefinedExecutionErrorIdentifier
    "PropertyIdentifier" -> Right AST.PropertyIdentifier
    "TypeIdentifier"     -> Right AST.TypeIdentifier
    "MetadataIdentifier" -> Right AST.MetadataIdentifier
    otherwise -> Left $ ParseError "FullyQualifiedIdentifier not available"

parseContentType :: X.Node -> Either ParseError AST.ContentType
parseContentType root = do
  ctType       <- findChild "Type" root >>= textParser
  ctSubtype    <- findChild "Subtype" root >>= textParser
  param        <- findChild "Parameters" root
  ctParameters <- sequence $ toParam <$> (X.children param)
  Right AST.ContentType{..}
  where
    toParam :: X.Node -> Either ParseError (T.Text, T.Text)
    toParam node = liftM2 (,)
      (findChild "Attribute" node >>= textParser)
      (findChild "Value" node >>= textParser)


parseSchemaConstraint :: X.Node -> Either ParseError AST.SchemaConstraint
parseSchemaConstraint root = do
  tyNode <- findChild "Type" root
  ty <- textParser tyNode >>= toType
  src <- (case filter (/= tyNode) childs of
    []  -> Left $ NodeNotFoundError "Url or Inline"
    [a] -> case X.name a of
      "Url"     -> AST.SCDUrl <$> textParser a
      "Inline"  -> AST.SCDInline <$> textParser a
      otherwise -> Left $ ParseError "Unallowed Option, valid (Url, Inline)"
    otherwise -> Left $ ParseError "No many childs")
  Right $ AST.SchemaConstraint ty src 
  where
    childs :: [X.Node]
    childs = X.children root
    toType :: T.Text -> Either ParseError AST.SchemaConstraintType
    toType t = case t of
      "Xml"  -> Right AST.SchemaConstraintXML
      "Json" -> Right AST.SchemaConstraintJSON
      otherwise -> Left $ ParseError "Schema Type not allowed (Xml, Json)"

parseUnit :: X.Node -> Either ParseError AST.Unit
parseUnit root = do
  unitLabel  <- findChild "Label" root >>= textParser
  unitFactor <- findChild "Factor" root >>= getContentText >>= (\v -> withParser v scientific)
  unitOffset <- findChild "Offset" root >>= getContentText >>= (\v -> withParser v scientific)
  unitComponent <- findChild "UnitComponent" root >>= toComponent
  Right AST.Unit{..}
  where
    toComponent :: X.Node -> Either ParseError [(AST.SIUnit, Int)]
    toComponent root = do
      unitsN <- findChilds "SIUnit" root
      units  <- sequence $ parseSIUnit <$> unitsN
      expN   <- findChilds "Exponent" root
      exp    <- sequence $ toInt <$> expN
      Right $ zipWith (,) units exp 
    toInt :: X.Node -> Either ParseError Int
    toInt node = do
      str <- getContentText node
      n   <- withParser str scientific
      case toBoundedInteger n of
        Nothing -> Left $ ParseError "Cant convert Scientific Expontent to Integer"
        Just i  -> Right i


parseSIUnit :: X.Node -> Either ParseError AST.SIUnit
parseSIUnit root = do
  unit <- getContentText root
  case unit of
    "Dimensionless" -> Right AST.SIUnitDimensionless
    "Meter"         -> Right AST.SIUnitMeter
    "Kilogram"      -> Right AST.SIUnitKilogram
    "Second"        -> Right AST.SIUnitSecond
    "Ampere"        -> Right AST.SIUnitAmpere
    "Kelvin"        -> Right AST.SIUnitKelvin
    "Mole"          -> Right AST.SIUnitMole
    "Candela"       -> Right AST.SIUnitCandela    

parseConstraints :: X.Node -> Either ParseError [AST.Constraint]
parseConstraints parent = sequence $ parseConstraint <$> X.children parent

parseConstrainedType :: X.Node -> Either ParseError AST.DataType -- TODO check length datatype and constraints
parseConstrainedType parent = do
  dTyp  <- parseDataType parent
  const <- findChild "Constraints" parent >>= parseConstraints
  return $ AST.ConstraintType dTyp const

parseDataTypeIdentifier :: X.Node -> Either ParseError AST.DataType
parseDataTypeIdentifier parent = do
  identifier <- E.decodeUtf8 <$> getContentText parent
  Right $ AST.IdentifierType identifier

parseDataTypeType :: X.Node -> Either ParseError AST.DataType
parseDataTypeType root = do
  child <- getSingleChild root
  case X.name child of
    "Basic"              -> AST.BasicType <$> parseBasicType child
    "List"               -> parseListType child
    "Structure"          -> parseStructureType child
    "Constrained"        -> parseConstrainedType child
    "DataTypeIdentifier" -> parseDataTypeIdentifier child 
    e                    -> Left $ ParseError $ T.unpack("parseDataTypeType " <> (E.decodeUtf8 e))

parseFeatureVersion :: X.Node -> Either ParseError AST.Version
parseFeatureVersion = parseVersion "FeatureVersion"

parseSiLAVersion :: X.Node -> Either ParseError AST.Version
parseSiLAVersion = parseVersion "SiLA2Version"

parseIdentifier :: X.Node -> Either ParseError T.Text
parseIdentifier parent = do
  identifier <- findChild "Identifier" parent 
  E.decodeUtf8 <$> getContentText identifier

parseDisplayName :: X.Node -> Either ParseError T.Text
parseDisplayName parent = do
  identifier <- findChild "DisplayName" parent 
  E.decodeUtf8 <$> getContentText identifier

parseDescription :: X.Node -> Either ParseError T.Text
parseDescription parent = do
  identifier <- findChild "Description" parent 
  E.decodeUtf8 <$> getContentText identifier

parseMaturityLevel :: X.Node -> Either ParseError AST.MaturityLevel
parseMaturityLevel root = case findAttr "MaturityLevel" root of
  Left _ -> Right $ AST.Draft -- defaults to
  Right attr ->  case attr of
    "Verified"  -> Right $ AST.Verified
    "Draft"     -> Right $ AST.Draft
    "Normative" -> Right $ AST.Normative
    e           -> Left $ ParseError $ T.unpack ("Not parseable " <> E.decodeUtf8 e)
  
parseOriginator :: X.Node -> Either ParseError T.Text
parseOriginator root = E.decodeUtf8 <$> (findAttr "Originator" root)

parseCategory :: X.Node -> Either ParseError T.Text
parseCategory root = E.decodeUtf8 <$> (findAttr "Category" root)

parseParameter :: X.Node -> Either ParseError [AST.Element]
parseParameter = parseElements "Parameter"

parseDataTypeDefinition :: X.Node -> Either ParseError [AST.Element]
parseDataTypeDefinition = parseElements "DataTypeDefinition"

parseResponse :: X.Node -> Either ParseError [AST.Element]
parseResponse = parseElements "Response"

parseIntermediateResponse :: X.Node -> Either ParseError [AST.Element]
parseIntermediateResponse = parseElements "IntermediateResponse"

parseDefinedExecutionErrors :: X.Node -> Either ParseError [AST.DefinedExecutionError]
parseDefinedExecutionErrors root = do
  childs <- findChilds "DefinedExecutionError" root
  sequence $ parseDefinedExecutionError <$> childs

parseDefinedExecutionError :: X.Node -> Either ParseError AST.DefinedExecutionError
parseDefinedExecutionError root = do
  deeIdentifier <-parseIdentifier root 
  deeDisplayName <- parseDisplayName root
  deeDescription <- parseDescription root 
  Right AST.DefinedExecutionError{..}

parseDefinedExecutionErrorList :: X.Node -> Either ParseError (Maybe [T.Text])
parseDefinedExecutionErrorList parent = case findChild "DefinedExecutionErrors" parent of
  Left _ -> Right Nothing
  Right child -> do
    errChilds <- findChilds "Identifier" child
    txErrors <- sequence $ getContentText <$> errChilds
    Right $ Just (E.decodeUtf8 <$> txErrors)

parseCommand :: X.Node -> Either ParseError AST.Command
parseCommand root = do
  commandIdentifier <- parseIdentifier root
  commandDisplayName <- parseDisplayName root
  commandDescription <- parseDescription root
  commandType        <- parseCommandType root
  commandParameter   <- parseParameter root
  commandResponse    <- parseResponse root
  commandIntermediateResponse   <- parseIntermediateResponse root
  commandDefinedExecutionErrors <- parseDefinedExecutionErrorList root
  return $ AST.Command{..}

parseCommands :: X.Node -> Either ParseError [AST.Command]
parseCommands parent = sequence $ parseCommand <$> filter (\n -> X.name n == "Command") (X.children parent)

parseElements :: ByteString -> X.Node -> Either ParseError [AST.Element]
parseElements elem parent = do
  elements <- findChilds elem parent
  sequence $ parseElement <$> elements


parseDataType :: X.Node -> Either ParseError AST.DataType
parseDataType parent = (findChild "DataType" parent) >>= parseDataTypeType


parseBasicType :: X.Node -> Either ParseError AST.BasicType
parseBasicType root =  case (getContentText root) of
  Left e -> Left e
  Right t -> case t of
    "String"    -> Right $ AST.TString
    "Integer"   -> Right $ AST.TInteger
    "Real"      -> Right $ AST.TReal
    "Boolean"   -> Right $ AST.TBoolean
    "Binary"    -> Right $ AST.TBinary
    "Date"      -> Right $ AST.TDate
    "Time"      -> Right $ AST.TTime
    "Timestamp" -> Right $ AST.TTimestamp
    "Any"       -> Right $ AST.TAny
    ty          -> Left $ ParseError (T.unpack $ "Type not known" <> E.decodeUtf8 ty)


parseProperties :: X.Node -> Either ParseError [AST.Property]
parseProperties parent = sequence $ parseProperty <$> filter (\n -> X.name n == "Property") (X.children parent)

parseProperty :: X.Node -> Either ParseError AST.Property
parseProperty parent = do
  propertyIdentifier <- parseIdentifier parent
  propertyDisplayName <- parseDisplayName parent
  propertyDescription <- parseDescription parent
  propertyObservable  <- parseCommandType parent
  propertyDataType    <- parseDataType parent
  propertyDefinedExecutionErrors <- parseDefinedExecutionErrorList parent
  return $ AST.Property{..}

parseMetadatas :: X.Node -> Either ParseError [AST.Metadata]
parseMetadatas parent = sequence $ parseMetadata <$> filter (\n -> X.name n == "Metadata") (X.children parent)
 
parseMetadata :: X.Node -> Either ParseError AST.Metadata
parseMetadata parent = do
  metaIdentifier <- parseIdentifier parent
  metaDisplayName <- parseDisplayName parent
  metaDescription <- parseDescription parent
  metaDataType    <- parseDataType parent
  metaDefinedExecutionError <- parseDefinedExecutionErrorList parent
  return $ AST.Metadata{..}
