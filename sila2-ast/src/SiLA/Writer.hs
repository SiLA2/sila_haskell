{-# language OverloadedStrings #-}
{-# language RecordWildCards #-}

module SiLA.Writer where

import SiLA.AST
import qualified Text.PrettyPrint as PP
import Control.Monad (forM)
import qualified Data.Text as T
import Text.PrettyPrint
import Text.PrettyPrint.HughesPJClass
import Data.Scientific (formatScientific, FPFormat(..))

import Debug.Trace

tText :: T.Text -> Doc
tText = text . T.unpack

pElement :: String -> T.Text -> Doc
pElement el content = start PP.<> tText content PP.<> stop
  where
    start = char '<'  PP.<> text el PP.<> char '>'
    stop  = text "</" PP.<> text el PP.<> char '>'

prologue :: Doc
prologue = text "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"

renderFeatureString :: Feature -> String
renderFeatureString = render . renderFeature

renderFeature :: Feature -> Doc
renderFeature f = prologue $$ pPrint f

instance Pretty Version where
  pPrint Version{..} = int versionMajor PP.<> "." PP.<> int versionMinor

instance Pretty MaturityLevel where
  pPrint Draft     = text "Draft"
  pPrint Verified  = text "Verified"
  pPrint Normative = text "Normative"

instance Pretty Observable where
  pPrint o = pElement "Observable" r
    where
      r = case o of
        Observable    -> "Yes"
        NonObservable -> "No"

toBlock :: Pretty a => String -> a -> Doc
toBlock name a= toBlock' name $ pPrint a

toBlock' :: String -> Doc -> Doc
toBlock' el b = start $$ nest 2 b $$ stop
  where
    start = char '<'  PP.<> text el PP.<> char '>'
    stop  = text "</" PP.<> text el PP.<> char '>'

instance Pretty BasicType where
  pPrint bt = pElement "Basic" ty
    where
      ty = case bt of
        TString    -> "String"
        TInteger   -> "Integer"
        TReal      -> "Real"
        TBoolean   -> "Boolean"
        TBinary    -> "Binary"
        TDate      -> "Date"
        TTime      -> "Time"
        TTimestamp -> "Timestamp"
        TAny       -> "Any"

instance Pretty DataType where
  pPrint (BasicType bt)       = pPrint bt
  pPrint (ListType l)         = toBlock' "List" (vcat $ (\d -> toBlock' "DataType" $ pPrint d) <$> l)
  pPrint (StructureType t)    = toBlock' "Structure" (vcat $ (\d -> toBlock' "Element" $ pPrint d) <$> t)
  pPrint (ConstraintType d c) = toBlock' "Constrained"
    (vcat [toBlock' "DataType" (pPrint d), toBlock' "Constraints" $ vcat (pPrint <$> c)])
  pPrint (IdentifierType t)   = pElement "DataTypeIdentifier" t

instance Pretty Constraint where
  pPrint (CLength l) = pElement "Length" (T.pack $ show l)
  pPrint (CMinimalLength l) = pElement "MinimalLength" (T.pack $ show l)
  pPrint (CMaximalLength l) = pElement "MaximalLength" (T.pack $ show l)
  pPrint (CSet s)           = toBlock' "Set" (vcat $ (pElement "Value") <$> s)
  pPrint (CPattern p)       = pElement "Pattern" p
  pPrint (CMaximalExclusive v) = pElement "MaximalExclusive" v
  pPrint (CMaximalInclusive v) = pElement "MaximalInclusive" v
  pPrint (CMinimalExclusive v) = pElement "MinimalExclusive" v
  pPrint (CMinimalInclusive v) = pElement "MinimalInclusive" v
  pPrint (CUnit u)             = pPrint u
  pPrint (CContentType c)      = pPrint c
  pPrint (CElementCount c)     = pElement "ElementCount" (T.pack $ show c)
  pPrint (CMinimalElementCount c) = pElement "MinimalElementCount" (T.pack $ show c)
  pPrint (CMaximalElementCount c) = pElement "MaximalElementCount" (T.pack $ show c)
  pPrint (CFullyQualifiedIdentifier i) = pPrint i
  pPrint (CSchema s)                   = pPrint s
  pPrint (CAllowedTypes a)             = toBlock' "AllowedTypes" $
    vcat ((\c -> toBlock' "DataType" $ pPrint c) <$> a) 


instance Pretty SchemaConstraint where
  pPrint SchemaConstraint{..} = toBlock' "Schema" $ vcat
    [ hcat [ptext "<Type>", t, "</Type>"]
    , pElement n c]
    where
     t = case scType of
       SchemaConstraintXML  -> ptext "Xml"
       SchemaConstraintJSON -> ptext "Json"
     (n, c) = case scData of
        (SCDInline c') -> ("Inline", c')
        (SCDUrl u')    -> ("Url", u') 

instance Pretty QualifiedIdentifier where
  pPrint FeatureIdentifier          = prettyQI "FeatureIdentifier"
  pPrint CommandIdentifier          = prettyQI "CommandIdentifier"
  pPrint CommandParameterIdentifier = prettyQI "CommandParameterIdentifier"
  pPrint CommandResponseIdentifier  = prettyQI "CommandResponseIdentifier"
  pPrint IntermediateCommandResponseIdentifier = prettyQI "IntermediateCommandResponseIdentifier"
  pPrint DefinedExecutionErrorIdentifier = prettyQI "DefinedExecutionErrorIdentifier"
  pPrint PropertyIdentifier         = prettyQI "PropertyIdentifier"
  pPrint TypeIdentifier             = prettyQI "TypeIdentifier"
  pPrint MetadataIdentifier         = prettyQI "MetadataIdentifier" 

prettyQI :: T.Text -> Doc
prettyQI = pElement "FullyQualifiedIdentifier" 

instance Pretty ContentType where
  pPrint ContentType{..} = toBlock' "ContentType" $ vcat
    [ pElement "Type" ctType
    , pElement "Subtype" ctSubtype
    , toBlock' "Parameter" ( vcat $ (\(a, v) ->
        pElement "Attribute" a $$ pElement "Value" v) <$> ctParameters)
    ]

instance Pretty SIUnit where
  pPrint (SIUnitDimensionless) = ptext "Dimensionless"
  pPrint (SIUnitMeter) = ptext "Meter"
  pPrint (SIUnitKilogram) = ptext "Kilogram"
  pPrint (SIUnitSecond) = ptext "Second"
  pPrint (SIUnitAmpere) = ptext "Ampere"
  pPrint (SIUnitKelvin) = ptext "Kelvin"
  pPrint (SIUnitMole)   = ptext "Mole"
  pPrint (SIUnitCandela)= ptext "Candela" 

instance Pretty Unit where
  pPrint Unit{..} = vcat
    [ text "<Unit>"
    , nest 2 $ vcat
      [ pElement "Label" unitLabel
      , pElement "Factor" (T.pack $ formatScientific Fixed Nothing unitFactor)
      , pElement "Offset" (T.pack $ formatScientific Fixed Nothing unitOffset)
      , toBlock' "UnitComponent" $ (vcat $ toComp <$> unitComponent)
      ]
    , text "</Unit>"
    ]
    where
      toComp :: (SIUnit, Int) -> Doc
      toComp (u,i) = vcat
        [ hcat [ptext "<SIUnit>", pPrint u, ptext "</SIUnit>"]
        , pElement "Exponent" (T.pack $ show i)]


instance Pretty Element where
  pPrint Element{..} = vcat
    [ pElement "Identifier"  elementIdentifier
    , pElement "DisplayName" elementDisplayName
    , pElement "Description" elementDescription
    , toBlock "DataType"     elementDataType
    ]
  
instance Pretty Metadata where
  pPrint Metadata{..} = vcat
    [ text "<Metadata>"
    , nest 2 $ vcat
      [ pElement "Identifier"  metaIdentifier
      , pElement "DisplayName" metaDisplayName
      , pElement "Description" metaDescription
      , toBlock "DataType"     metaDataType
      , renderDefinedExecutionErrors metaDefinedExecutionError
      ]
    , text "</Metadata>"
    ]

instance Pretty Property where
  pPrint Property{..} = vcat
    [ text "<Property>"
    , nest 2 $ vcat
      [ pElement "Identifier"  propertyIdentifier
      , pElement "DisplayName" propertyDisplayName
      , pElement "Description" propertyDescription
      , pPrint propertyObservable
      , toBlock "DataType"     propertyDataType
      , renderDefinedExecutionErrors propertyDefinedExecutionErrors
      ]
    , text "</Property>"
    ]

instance Pretty Command where
  pPrint Command{..} = vcat
    [ text "<Command>"
    , nest 2 $ vcat
      [ pElement "Identifier" commandIdentifier
      , pElement "DisplayName" commandDisplayName
      , pElement "Description" commandDescription
      , pPrint commandType
      , vcat $ toBlock "Parameter"  <$> commandParameter
      , vcat $ toBlock "Response"  <$> commandResponse
      , vcat $ toBlock "IntermediateResponse"  <$> commandIntermediateResponse
      , renderDefinedExecutionErrors commandDefinedExecutionErrors
      ]
    , text "</Command>"
    ]

renderDataTypeDefinition :: [Element] -> Doc
renderDataTypeDefinition l = vcat $ (toBlock "DataTypeDefinition") <$> l

renderDefinedExecutionErrors :: Maybe [T.Text] -> Doc
renderDefinedExecutionErrors l = case l of 
        Nothing -> empty
        (Just l) -> vcat
          [ text "<DefinedExecutionErrors>"
          , nest 2 (vcat $ (\e -> pElement "Identifier" e) <$> l)
          , text "</DefinedExecutionErrors>"
          ]

instance Pretty Feature where
  pPrint Feature{..} = vcat
    [ text "<Feature"
        <+> "SiLA2Version="   PP.<> doubleQuotes (pPrint featureSiLA2Version) 
        <+> "FeatureVersion=" PP.<> doubleQuotes (pPrint featureVersion) 
        <+> "MaturityLevel="  PP.<> doubleQuotes (pPrint featureMaturityLevel)
        <+> "Originator="     PP.<> doubleQuotes (tText featureOriginator)
        <+> "Category="       PP.<> doubleQuotes (tText featureCategory)
        <+> "xmlns=\"http://www.sila-standard.org\""
        <+> "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
        <+> "xsi:schemaLocation=\"http://www.sila-standard.org" 
          <+> "https://gitlab.com/SiLA2/sila_base/raw/master/schema/FeatureDefinition.xsd\">"
    , nest 2 $ vcat
        [ pElement "Identifier" featureIdentifier
        , pElement "DisplayName" featureDisplayName
        , pElement "Description" featureDescription
        , vcat (pPrint <$> featureCommand)
        , vcat (pPrint <$> featureProperty)
        , vcat (pPrint <$> featureMetadata)
        , vcat (pPrint <$> featureDefinedExecutionError)
        , renderDataTypeDefinition featureDataTypeDefinition
        ]
    , text "</Feature>"
    ]


instance Pretty DefinedExecutionError where
  pPrint DefinedExecutionError{..} = toBlock' "DefinedExecutionError" $ vcat
    [ pElement "Identifier"  deeIdentifier
    , pElement "DisplayName" deeDisplayName
    , pElement "Description" deeDescription
    ]


