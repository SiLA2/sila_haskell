{-# language DerivingStrategies #-}
module SiLA.AST where


import qualified Data.Text as T
import Data.Scientific (Scientific)

data Feature
  = Feature
  { featureIdentifier    :: !T.Text -- https://gitlab.com/SiLA2/sila_base/-/blob/master/schema/DataTypes.xsd#L17
  , featureDisplayName   :: !T.Text -- https://gitlab.com/SiLA2/sila_base/-/blob/master/schema/DataTypes.xsd#L17
  , featureDescription   :: !T.Text
  , featureCommand       :: [Command]
  , featureProperty      :: [Property]
  , featureMetadata      :: [Metadata]
  , featureDefinedExecutionError :: [DefinedExecutionError]
  , featureDataTypeDefinition    :: [Element]
  , featureSiLA2Version  :: !Version -- https://gitlab.com/SiLA2/sila_base/-/blob/master/schema/FeatureDefinition.xsd#L82
  , featureVersion       :: !Version
  , featureMaturityLevel :: !MaturityLevel -- defaults to Draft
  , featureOriginator    :: !T.Text
  , featureCategory      :: !T.Text
  } deriving stock (Show, Eq)

data DefinedExecutionError
  = DefinedExecutionError
  { deeIdentifier  :: !T.Text
  , deeDisplayName :: !T.Text
  , deeDescription :: !T.Text
  } deriving stock (Show, Eq)

data Property
  = Property
  { propertyIdentifier  :: !T.Text
  , propertyDisplayName :: !T.Text
  , propertyDescription :: !T.Text
  , propertyObservable  :: !Observable
  , propertyDataType    :: !DataType
  , propertyDefinedExecutionErrors :: (Maybe [T.Text])
  } deriving stock (Show, Eq)


data Metadata
  = Metadata
  { metaIdentifier  :: !T.Text
  , metaDisplayName :: !T.Text
  , metaDescription :: !T.Text
  , metaDataType    :: !DataType
  , metaDefinedExecutionError :: (Maybe [T.Text])
  } deriving stock (Show, Eq)


data MaturityLevel
  = Draft
  | Verified
  | Normative
  deriving stock (Show, Eq)

data Version
  = Version
  { versionMajor :: !Int
  , versionMinor :: !Int
  } deriving stock (Show, Eq)

data Observable = Observable | NonObservable
  deriving stock (Show, Eq)

data Command
  = Command
  { commandIdentifier  :: !T.Text
  , commandDisplayName :: !T.Text
  , commandDescription :: !T.Text
  , commandType        :: !Observable
  , commandParameter   :: [Element]
  , commandResponse    :: [Element]
  , commandIntermediateResponse   :: [Element]
  , commandDefinedExecutionErrors :: (Maybe [T.Text]) -- https://gitlab.com/SiLA2/sila_base/-/blob/master/schema/FeatureDefinition.xsd#L122
  } deriving stock (Show, Eq)

data BasicType
  = TString
  | TInteger
  | TReal
  | TBoolean
  | TBinary
  | TDate
  | TTime
  | TTimestamp
  | TAny
  deriving stock (Show, Eq)

-- https://gitlab.com/SiLA2/sila_base/-/blob/master/schema/DataTypes.xsd#L8
data DataType
  = BasicType !BasicType
  | ListType [DataType]
  | StructureType [Element]
  | ConstraintType DataType [Constraint]
  | IdentifierType !T.Text
  deriving stock (Show, Eq)


-- Constraints
--
-- https://gitlab.com/SiLA2/sila_base/-/blob/master/schema/Constraints.xsd
--

data Constraint
  = CLength !Int
  | CMinimalLength !Int
  | CMaximalLength !Int
  | CSet [T.Text] -- NonEmpty list
  | CPattern !T.Text
  | CMaximalExclusive !T.Text -- hm ? should be BasicType 
  | CMaximalInclusive !T.Text
  | CMinimalExclusive !T.Text
  | CMinimalInclusive !T.Text
  | CUnit !Unit
  | CContentType !ContentType
  | CElementCount !Int
  | CMinimalElementCount !Int
  | CMaximalElementCount !Int
  | CFullyQualifiedIdentifier !QualifiedIdentifier
  | CSchema !SchemaConstraint
  | CAllowedTypes [DataType]
  deriving stock (Show, Eq)

data SchemaConstraintType
  = SchemaConstraintXML
  | SchemaConstraintJSON
  deriving stock (Show, Eq)

data SchemaConstraintData
  = SCDInline !T.Text
  | SCDUrl !T.Text
  deriving stock (Show, Eq)

data SchemaConstraint
  = SchemaConstraint
  { scType :: !SchemaConstraintType
  , scData :: !SchemaConstraintData
  } deriving stock (Show, Eq)

data QualifiedIdentifier
  = FeatureIdentifier
  | CommandIdentifier
  | CommandParameterIdentifier
  | CommandResponseIdentifier
  | IntermediateCommandResponseIdentifier
  | DefinedExecutionErrorIdentifier
  | PropertyIdentifier
  | TypeIdentifier
  | MetadataIdentifier
  deriving stock (Show, Eq)

data ContentType
  = ContentType
  { ctType       :: !T.Text
  , ctSubtype    :: !T.Text
  , ctParameters :: [(T.Text, T.Text)] -- (attribute, value)
  } deriving stock (Show, Eq)

data Unit
  = Unit
  { unitLabel     :: !T.Text
  , unitFactor    :: !Scientific
  , unitOffset    :: !Scientific
  , unitComponent :: [(SIUnit, Int)] -- https://gitlab.com/SiLA2/sila_base/-/blob/master/schema/Constraints.xsd#L86
  } deriving stock (Show, Eq)

data SIUnit
  = SIUnitDimensionless
  | SIUnitMeter
  | SIUnitKilogram
  | SIUnitSecond
  | SIUnitAmpere
  | SIUnitKelvin
  | SIUnitMole
  | SIUnitCandela
  deriving stock (Show, Eq)

data Element
  = Element
  { elementIdentifier  :: !T.Text
  , elementDisplayName :: !T.Text
  , elementDescription :: !T.Text
  , elementDataType    :: !DataType
  } deriving stock (Show, Eq)
