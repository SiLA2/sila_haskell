{-# language OverloadedStrings #-}

module Main where

import Test.Hspec
import System.Directory
import qualified SiLA.Parser as P
import SiLA.AST
import SiLA.Writer
import Control.Monad (forM_, liftM2)
import Data.Either (isRight, fromRight)
import System.Exit
import System.Process
import System.IO.Temp
import qualified Data.ByteString as BS
import qualified Xeno.DOM as X
import Debug.Trace

report :: FilePath -> IO ()
report file  = do
  result <- P.parseFile ("example/" <> file)
  case result of
    Left e -> putStrLn $ file <> " failed to parse (" <> show e <> ")"
    Right _ -> putStrLn $ "\t" <> file <> " parsed!"

validateFileSchema :: FilePath -> IO ExitCode
validateFileSchema file = system $
  "xmllint --noout --schema ../sila_base/schema/FeatureDefinition.xsd " ++ file ++ " 2> /dev/null"

validateFileNormalized :: FilePath -> FilePath -> IO ExitCode
validateFileNormalized orig gen = do
    system $ "./tests/xml-norm.sh " ++ gen  ++ " _tmp1 2> /dev/null"
    system $ "./tests/xml-norm.sh " ++ orig ++ " _tmp2 2> /dev/null" 
    r <- system $ "diff _tmp1 _tmp2 | grep -v '<!--' 2> /dev/null"
    system $ "rm _tmp1 _tmp2"
    return r


testFile :: FilePath -> Spec
testFile p =  do
  files <- runIO $ listDirectory p
  forM_ files $ (\f -> do
    let filePath = p <> f
    describe filePath $ do
      it "parse file" $ do
        content <-  P.parseFile filePath
        isRight content `shouldBe` True

      it "validate against schema" $ do
        let filePath = p <> f
        (Right content) <- P.parseFile filePath
        let out = renderFeatureString content
        testFile <- writeTempFile "/tmp" "test" out
        result <- validateFileSchema testFile
        removeFile testFile
        result `shouldBe` ExitSuccess

      it "xml equality" $ do
        let filePath = p <> f
        (Right content) <- P.parseFile filePath
        let out = renderFeatureString content
        testFile <- writeTempFile "/tmp" "test" out
        result <- validateFileNormalized filePath testFile
        removeFile testFile
        result `shouldBe` ExitSuccess  
    )
    
main :: IO ()
main = hspec $ do
  testFile "example/"
  testFile "../sila_base/feature_definitions/org/silastandard/test/"
